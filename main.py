#!/usr/bin/env python
# -*- coding: utf-8 -*-

import subprocess as spr
from docx import Document
import os
import json
import docx2txt
from flask import request, url_for, jsonify

from read_services.config import *
from read_services import app


def getText(filename):
    doc = Document(filename)
    fullText = []
    for para in doc.paragraphs:
        fullText.append(para.text.replace("\n", ""))
    return "\n".join(fullText)


def handling_docx(file_name=None, input_dirs=INPUT_DIR, output_dir=OUTPUT_DIR, image_dir=IMAGE_DIR):
    current_dir = os.path.dirname(os.path.abspath(__file__))

    try:
        os.stat(output_dir)
    except Exception:
        os.mkdir(output_dir)

    try:
        os.stat(image_dir)
    except Exception:
        os.mkdir(image_dir)

    if file_name:
        input_files = [os.path.join(input_dirs[0], file_name)]
    else:
        input_files = [os.path.join(input_dir, f)
                for input_dir in input_dirs
                for f in os.listdir(input_dir)
                if os.path.isfile(os.path.join(input_dir, f))]

    for docx_file in input_files:
        text = getText(docx_file)
        questions_ = text.split(SEPARATE)

        lst_questions_img = sorted(set([i.strip() for i in text.split(LST_QUESTIONS_IMG)[1].split("\n")[0].split(",")]), key=lambda x: int(x))

        file_name = docx_file.rsplit("/", 1)[1].split(".docx")[0]
        if file_name not in os.listdir(image_dir):
            os.mkdir(os.path.join(image_dir, file_name))
        image_dir_child = os.path.join(image_dir, file_name)
        docx2txt.process(docx_file, image_dir_child)

        lst_img = sorted([f.split(".")[0] for f in os.listdir(image_dir_child) if os.path.isfile(os.path.join(image_dir_child, f))], key=lambda x : int(x.split("image")[1]))
        zip_imgs_questions = zip(lst_questions_img, lst_img)
        check_img_question = {}
        for u, v in zip_imgs_questions:
            check_img_question[u] = v

        data = []
        count = 1
        for qt in questions_:
            data_ = {}
            answer = []

            check_img = check_img_question.get(str(count))
            if check_img:
                for image_name_ in os.listdir(image_dir_child):
                    if image_name_.startswith(check_img):
                        data_["image"] = os.path.join(image_dir_child, image_name_)

            step = ""
            handling_question = qt.split("\n")
            for element in handling_question:
                if element.startswith(HEAD_QUESTION):
                    step = "quest"
                    data_["question"] = element.strip()
                elif element.startswith(HEAD_CHOICE):
                    step = "choice"
                    data_["choice"] = element.strip()
                elif CURRENT_CHOICE in element:
                    data_["current_choice"] = element.strip()
                    answer.append(element.strip())
                else:
                    if element and step == "quest":
                        answer.append(element.strip())
                    if element and step == "choice":
                        data_["choice"] += "\n" + element.strip()
            data_["question_answer"] = answer
            count += 1
            data.append(data_)
        questions_handled = {"questions": data}
        file_name_json = file_name + ".json"
        file_json = os.path.join(output_dir, file_name_json)
        with open(file_json, 'w') as f_json:
            json.dump(questions_handled, f_json)
    return questions_handled


@app.route("/convert/docx", methods=["GET", "POST"])
def convert_docx():
    file_name = request.data.get("file_name")
    input_dirs = request.data.get("input_dir")
    output_dir = request.data.get("output_dir")
    image_dir = request.data.get("image_dir")

    try:
        os.stat(input_dirs[0])
    except Exception:
        return jsonify(msg="Thư mục chứa file đầu vào không tồn tại!")

    if file_name not in os.listdir(input_dirs[0]):
        return jsonify(msg="File không tồn tại!")

    data = handling_docx(file_name=file_name,
                      input_dirs=input_dirs,
                      output_dir=output_dir,
                      image_dir=image_dir
                )
    return jsonify(questions=data["questions"])


if __name__ == "__main__":
    app.run(debug=True)

